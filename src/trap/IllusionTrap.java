package trap;

import characters.Pool;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Encounter;
import global.Scheduler;
import status.Flatfooted;

public class IllusionTrap extends Trap {
	private Character owner;
	public IllusionTrap(){
		super("Illusion Trap","A seductive image that will arouse the victim","You cast a simple illusion that will trigger when someone approaches and seduce them.",3);
		cost.put(Pool.MOJO,15);
	}
	@Override
	public void trigger(Character target) {
		if(target.human()){
			Global.gui().message("You run into a girl you don't recognize, but she's beautiful and completely naked. You don't have a chance to wonder where she came from, because " +
					"she immediately presses her warm, soft body against you and kisses you passionately. She slips a hand between you to grope your crotch and suddenly vanishes. " +
					"She was just an illusion, but your erection is very real.");
		}
		else if(target.location().humanPresent()){
			Global.gui().message("There's a flash of pink light and "+target.name()+" flushes with arousal");
		}
		target.tempt(25);
		target.location().opportunity(target,this);
		cost.put(Pool.MOJO,15);
	}

	@Override
	public boolean requirements(Character owner) {
		return owner.getPure(Attribute.Arcane)>=6;
	}

	@Override
	public Trap copy() {
		return new IllusionTrap();
	}

}
