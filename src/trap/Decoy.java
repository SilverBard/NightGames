package trap;

import items.Component;
import items.Consumable;
import items.Item;
import global.Global;
import combat.Encounter;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class Decoy extends Trap {
	public Decoy(){
		super("Decoy","Makes enough noise to lure nearby opponents","You program a phone to play a prerecorded audio track five minutes from now. It should be noticable from a reasonable distance until someone switches it " +
				"off.",2);
		decoy = true;
		recipe.put(Component.Phone,1);
	}

	@Override
	public void trigger(Character target) {
		if(target.human()){
			Global.gui().message("You follow the noise you've been hearing for a while, which turns out to be coming from a disposable cell phone. Seems like someone " +
					"is playing a trick and you fell for it. You shut off the phone and toss it aside.");
		}
		else if(target.location().humanPresent()){
			Global.gui().message(target.name()+" finds the decoy phone and deactivates it.");
		}
		target.location().remove(this);
	}

	@Override
	public boolean requirements(Character owner) {
		return owner.getPure(Attribute.Cunning)>=7 && !owner.has(Trait.direct);
	}

	@Override
	public Trap copy() {
		return new Decoy();
	}


}
