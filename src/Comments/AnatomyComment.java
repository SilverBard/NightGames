package Comments;

import characters.Anatomy;
import characters.Character;
import combat.Combat;
import combat.CombatEvent;
import combat.Result;

public class AnatomyComment implements CommentTrigger {
    private int priority;
    private int probability;
    private Result event;
    private Anatomy part;
    private static final int DEFAULTPRIORITY = 2;
    private static final int DEFAULTPROBABILITY = 50;

    public AnatomyComment(Result event, Anatomy part){
        this(DEFAULTPRIORITY,DEFAULTPROBABILITY,event,part);
    }

    public AnatomyComment(int priority, int probability, Result event, Anatomy part){
        this.priority = priority;
        this.probability = probability;
        this.event = event;
        this.part = part;
    }

    @Override
    public boolean isApplicable(Combat c, Character self, Character other) {
        for(CombatEvent e: c.getEvents(self)){
            if(e.getEvent()== event && e.getPart()==part){
                return true;
            }
        }
        return false;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public int getProbability() {
        return probability;
    }

    @Override
    public boolean equals(Object o) {

        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        /* Check if o is an instance of SkillComment or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof AnatomyComment)) {
            return false;
        }

        // typecast o to SkillComment so that we can compare data members
        AnatomyComment c = (AnatomyComment) o;

        // Compare the data members and return accordingly
        return c.event == event && c.part == part;
    }
}
