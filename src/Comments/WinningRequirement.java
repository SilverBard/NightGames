package Comments;
import characters.Character;
import combat.Combat;

public class WinningRequirement implements CustomRequirement {

	private static final int INITIAL_WINNING_SCORE = 0;
	
	@Override
	public boolean meets(Combat c, Character self, Character other) {
		if (c == null)
			return false;
		int score = INITIAL_WINNING_SCORE;
		score += (other.getArousal().percent() - self.getArousal().percent()) / 2;
		if (c.stance.dom(self))
			score += 10;
		if (c.stance.dom(other))
			score -= 10;
		return score >= 20;
	}

}