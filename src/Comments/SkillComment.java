package Comments;

import characters.Character;
import combat.Combat;
import combat.CombatEvent;
import combat.Result;
import combat.Tag;

public class SkillComment implements CommentTrigger{
    private int priority;
    private int probability;
    private Tag tag;
    private boolean received;
    private static final int DEFAULTPRIORITY = 3;
    private static final int DEFAULTPROBABILITY = 80;

    public SkillComment(Tag tag){
        this(DEFAULTPRIORITY,DEFAULTPROBABILITY,tag,false);
    }
    public SkillComment(Tag tag, boolean received){
        this(DEFAULTPRIORITY,DEFAULTPROBABILITY,tag,received);
    }
    public SkillComment(int priority, int probability, Tag tag){
        this(priority,probability,tag,false);
    }

    public SkillComment(int priority, int probability, Tag tag, boolean received){
        this.priority = priority;
        this.probability = probability;
        this.tag = tag;
        this.received = received;
    }

    @Override
    public boolean isApplicable(Combat c, Character self, Character other) {
        if(received){
            for(CombatEvent event: c.getEvents(self)){
                if(event.getEvent()== Result.receiveskill && event.getSkill().hasTag(tag)){
                    return true;
                }
            }
        }else{
            for(CombatEvent event: c.getEvents(self)){
                if(event.getEvent()== Result.useskill && event.getSkill().hasTag(tag)){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public int getProbability() {
        return probability;
    }

    @Override
    public boolean equals(Object o) {

        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        /* Check if o is an instance of SkillComment or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof SkillComment)) {
            return false;
        }

        // typecast o to SkillComment so that we can compare data members
        SkillComment c = (SkillComment) o;

        // Compare the data members and return accordingly
        return c.tag == tag && c.received == received;
    }
}
