package characters;

import Comments.CommentGroup;
import Comments.CommentSituation;
import Comments.SkillComment;
import actions.Action;
import actions.Movement;
import combat.Combat;
import combat.Tag;
import daytime.Daytime;
import global.*;
import items.Clothing;
import items.Toy;
import items.Trophy;
import scenes.SceneFlag;
import scenes.SceneManager;
import skills.*;
import status.Stsflag;

import java.util.ArrayList;
import java.util.HashSet;

public class Sofia implements Personality {
    public NPC character;
    public Sofia(){
        character = new NPC("Sofia",ID.SOFIA,1,this);
        resetOutfit();
        character.closet.add(Clothing.bra);
        character.closet.add(Clothing.tanktop);
        character.closet.add(Clothing.panties);
        character.closet.add(Clothing.shorts);
        character.change(Modifier.normal);
        character.mod(Attribute.Power, 2);
        character.mod(Attribute.Speed, 1);
        character.mod(Attribute.Footballer, 2);
        character.getStamina().gainMax(30);
        Global.gainSkills(character);
        character.add(Trait.female);
        character.add(Trait.wrassler);
        character.add(Trait.achilles);
        character.add(Trait.striker);
        character.setUnderwear(Trophy.SofiaTrophy);
        character.plan = Emotion.hunting;
        character.mood = Emotion.confident;
        character.strategy.put(Emotion.sneaking, 1);
        character.strategy.put(Emotion.bored, 1);
        character.preferredSkills.add(Kick.class);
        character.preferredSkills.add(LowBlow.class);
        character.preferredSkills.add(Fuck.class);
        character.preferredSkills.add(Footjob.class);
        character.preferredSkills.add(OpenGoal.class);
        character.preferredSkills.add(KissBetter.class);
    }
    @Override
    public Skill act(HashSet<Skill> available, Combat c) {
        HashSet<Skill> mandatory = new HashSet<Skill>();
        HashSet<Skill> tactic = new HashSet<Skill>();
        Skill chosen;
        for(Skill a:available){
            if(character.is(Stsflag.orderedstrip)){
                if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
                    mandatory.add(a);
                }
            }
        }
        if(!mandatory.isEmpty()){
            Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
            return actions[Global.random(actions.length)];
        }
        ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
        if(Global.checkFlag(Flag.hardmode)&& !c.hasModifier(Modifier.quiet)){
            chosen = character.prioritizeNew(priority,c);
        }
        else{
            chosen = character.prioritize(priority);
        }
        if(chosen==null){
            tactic=available;
            Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
            return actions[Global.random(actions.length)];
        }
        else{
            return chosen;
        }
    }

    @Override
    public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
        Action proposed = character.parseMoves(available, radar, match);
        return proposed;
    }

    @Override
    public void rest(int time, Daytime day) {
        if(!(character.has(Toy.Crop)||character.has(Toy.Crop2))&&character.money>=200){
            character.gain(Toy.Crop);
            character.money-=200;
        }
        character.visit(4);
        String loc;
        ArrayList<String> available = new ArrayList<String>();
        available.add("XXX Store");
        available.add("Bookstore");
        if(character.rank>0){
            available.add("Workshop");
        }
        available.add("Play Video Games");
        for(int i=0;i<time-5;i++){
            loc = available.get(Global.random(available.size()));
            day.visit(loc, character, Global.random(character.money));
        }
        if(character.getAffection(Global.getPlayer())>0){
            Global.modCounter(Flag.SofiaDWV, 1);
        }
    }

    @Override
    public String bbLiner() {
        switch(Global.random(3)){
            case 2:
                return "<i>\"Oooooh, that must hurt sooo bad!\"</i> Sofia trills holding her mound and leaning in with an excited smirk as you clutch your aching balls.  You're not sure if she's mocking you, or sympathizing.";
            case 1:
                return "<i>\"Awww, do you need a moment to hold them?\"</i> Sofia purrs, standing in front of you with a mocking smirk, her hands on her hips and thighs confidently parted.  <i>\"Maybe wear a cup next time.\"</i>";
            default:
                return "You moan in agony and curl over at the hips, eyes screwed shut as you cup your throbbing balls. You hear a soft, feminine moan, and open your eyes to find Sofia's slender mocha fingers " +
                        "drawing firm circles around her moist sex, her bright eyes locked onto you with unnerving intensity.  <i>\"I don't know what's sexier… crushing your cojones, or watching you clutch them after…,\"</i> she purrs in a husky whisper, taking in your predicament with unconcealed lust.";
        }
    }

    @Override
    public String nakedLiner() {
        return "Sofia turns a little to the side with a coy smirk, reaching down to cup her plump venus mound in both hands. <i>\"Mmmm, you wouldn't hit me down there... would you?\"</i> she purrs, a naughty, hopeful smile tugging at her lips.";
    }

    @Override
    public String stunLiner() {
        return "<i>\"Oooooh! You got me so good!\"</i>";
    }

    @Override
    public String taunt() {
        return "<i>\"We could just skip to the end, where you beg me for mercy, but the journey's my favorite part.\"</i>";
    }

    @Override
    public void victory(Combat c, Tag flag) {
        Character opponent = c.getOther(character);
        character.clearGrudge(opponent);
        if(opponent.human()) {
            SceneManager.play(SceneFlag.SofiaForeplayVictory);
        }
    }

    @Override
    public void defeat(Combat c, Tag flag) {
        Character opponent = c.getOther(character);
        declareGrudge(opponent,c);
        if(opponent.human()) {
            SceneManager.play(SceneFlag.SofiaForeplayDefeat);
        }
    }

    @Override
    public String victory3p(Combat c, Character target, Character assist) {
        character.clearGrudge(target);
        character.clearGrudge(assist);
        if(target.human()){
            return "Sofia gently rolls you onto your back, and "+assist.name()+" straddles your face, holding your arms down with her body weight.  " +
                    "Sofia positions herself over your hips and effortlessly slides your cock inside of her; despite her wetness, her tight pussy grips " +
                    "your shaft firmly as she begins to gleefully bounce her generous ass up and down on you.  Unconsciously, your tongue begins to explore " +
                    assist.name()+"'s lips and clit, and she moans softly in approval.  Sofia and "+assist.name()+" are face-to-face as they each ride you, " +
                    "and Sofia brings "+assist.name()+" close to her and kisses her gently while fondling her breasts – "+assist.name()+" responds in kind, " +
                    "rubbing Sofia's slippery clit.  With your nose and mouth buried in "+assist.name()+", and Sofia enthusiastically riding your cock, " +
                    "it doesn't take long for you to explode, shooting an ample load up into Sofia.  A moment after your climax, you feel "+assist.name() +
                    "tense up, squeezing your head between her thighs with a moan, and shuddering with an orgasm of her own.  Almost instantly, Sofia gives " +
                    "in to the work of "+assist.name()+"'s curious fingers, and you feel her womanhood tense and contract around your cock as she climaxes as well. ";
        }else{
            return "You and Sofia have "+target.name()+" right where you want her.  You stand behind "+target.name()+" and bend her slightly forwards; " +
                    "Sofia stands in front of her and captures her with a passionate kiss, interlocking her fingers with "+target.name()+"'s.  You insert " +
                    "yourself into "+target.name()+" from behind, holding onto her hips to stabilize both of you as you begin to gently thrust rhythmically " +
                    "inside of her.  Keeping her fingers interlocked with "+target.name()+"'s, Sofia kneels down in front of her and begins licking and " +
                    "sucking her clit as you penetrate her, and you can feel her tongue also caress the base of your dick and balls.  Before long, " +
                    target.name()+" is wriggling and squirming in ecstasy, and she squeals and convulses in orgasm.  Sofia turns her attention from " +
                    target.name()+"'s clit to your balls, and begins sucking them eagerly – she rolls your balls around inside of her mouth as they lurch " +
                    "and contract, and you can still feel "+target.name()+" constricting around your shaft as you violently erupt inside of her. ";
        }
    }

    @Override
    public String intervene3p(Combat c, Character target, Character assist) {
        if(target.human()){
            return "Your match with "+assist.name()+" has been hard-fought, but you feel like you are running out of steam; you have both stripped each other " +
                    "completely, and you stand face-to-face, grappling desperately for the upper hand.  With a smirk, "+assist.name()+" sweeps her leg out " +
                    "and kicks the inside of your foot, spreading your legs wide open and throwing you off-balance.  However, the kick also left "+assist.name() +
                    " off balance and you are able to knock her down while staying on your feet. You seem to have to advantage, until you feel a foot slam " +
                    "into your dangling ballsack from behind.  As you crumple with a whimper, you catch sight of Sofia standing behind you, looking pleased " +
                    "with herself and savoring the expression on your face.  "+assist.name()+" must have seen her coming and set you up for the perfect kick.  " +
                    "As you hit the ground, the two of them waste no time and pounce on you.";
        }else{
            return "Your match with "+target.name()+" has been uncomfortably close, and you are desperate for an edge.  While locked up in a tight grapple with " +
                    target.name()+", you see Sofia cautiously approaching the two of you; exchanging a knowing glance with Sofia, you realize that you have " +
                    "just found the opportunity you need to win.  Naked and exhausted, you quickly spin "+target.name()+" around so you are grabbing her from " +
                    "behind with her arms pinned at her sides, and you nestle your erection snugly between her butt cheeks as she struggles against your hold.  " +
                    target.name()+" is too distracted to notice Sofia as she darts out in front of the two of you, and kicks her foot swiftly up between both " +
                    "of your legs, simultaneously kicking "+target.name()+" in the pussy with her shin and kicking you in the balls with her foot.  You and " +
                    target.name()+" collapse on top of each other on the ground with a groan, each clutching your pulverized privates, though it seems " +
                    target.name()+" got it far worse than you did.  <i>\"Sorry, "+assist.name()+", I couldn't resist,\"</i> Sofia says, helping you to your feet with a smile " +
                    "and a wink.  <i>\"Now let's finish this, OK?\"</i>";
        }
    }

    @Override
    public String resist3p(Combat c, Character target, Character assist) {
        return null;
    }

    @Override
    public void watched(Combat c, Character target, Character viewer) {
        if(viewer.human()) {
            SceneManager.play(SceneFlag.SofiaWatch);
        }
    }

    @Override
    public String describe() {
        return "Sofia looks like a natural athlete. It's in everything about her, from her thick, steely thighs, to the smooth, kinetic quality of her every movement. Her figure is lithe yet voluptuous, with slender arms and a pinched midriff that offsets her prominent D-cup breasts, wide hips, and full bubble-butt. Her wavy, dirty blonde locks strike quite the contrast against her soft, glistening mocha skin, and her large, cheerful brown eyes match her bouncy, yet fiery personality.";
    }

    @Override
    public void draw(Combat c, Tag flag) {
        Character opponent = c.getOther(character);
        character.clearGrudge(opponent);
        if(opponent.human()) {
            SceneManager.play(SceneFlag.SofiaForeplayDraw);
        }
    }

    @Override
    public boolean fightFlight(Character opponent) {
        return fit();
    }

    @Override
    public boolean attack(Character opponent) {
        return true;
    }

    @Override
    public void ding() {
        character.mod(Attribute.Footballer, 1);
        int rand;
        character.getStamina().gainMax(6);
        character.getArousal().gainMax(4);
        character.getMojo().gainMax(2);
        for(int i=0; i<(Global.random(3)/2)+1;i++){
            rand=Global.random(3);
            if(rand==0){
                character.mod(Attribute.Power, 1);
            }
            else if(rand==1){
                character.mod(Attribute.Seduction, 1);
            }
            else if(rand==2){
                character.mod(Attribute.Cunning, 1);
            }
        }
    }

    @Override
    public String startBattle(Character opponent) {
        if(character.getGrudge()!=null) {
            switch (character.getGrudge()) {
                case defensivemeasures:
                    return "Sofia struts up with her usual confidence and intensity, but her stride's... off. Her hips aren't quite swaying as much as usual, and " +
                            "she seems just a bit bow-legged. She stands across from you, confidently parting her thighs with her hands on her hips, her mound " +
                            "seems even a little more pronounced than usual. She smirks, following your gaze, and the sound of hollow plastic as she raps her " +
                            "knuckles against her groin confirms your suspicions. <i>\"Let's see what you can do with my weakness covered up. Getting hit down there " +
                            "makes me hot, but tonight I'm here to beat you!\"</i>";
                case sadisticmood:
                    return "Sofia smirks as she squares up against you, sensually licking her lips as she eyes you up and down, her eyes lingering on your groin. " +
                            "Knowing her proclivities, the intensity of her stare is making you feel pretty uncomfortable. <i>\"The only thing " +
                            "that turns me on more than a big, strong man rolling on the ground and holding his throbbing balls, is you,\"</i> she whispers, " +
                            "<i>\"Rolling on the ground and holding your throbbing balls.\"</i>";
                case cheapshot:
                    return "Sofia's eyes are filled with lust, and as she makes a beeline for you she seems absolutely ready to jump your bones on the spot. You get " +
                            "ready to fight as she closes the distance, but lose your train of thought as she yanks her top off mid-stride. Your eyes rest on her full, " +
                            "heaving bust as it seemingly tries to burst out of her sports bra. She grasps your shoulders and leans in for a kiss, but just as her lips " +
                            "brush yours, your testicles explode into agony! Sofia smirks as you totter backwards, hunched over and cupping your aching balls. <i>\"What, " +
                            "aren't we fighting?\"</i> she coos with a sly grin.";
                default:
                    break;
            }
        }if(opponent.nude()){
            return "Sofia eyes you with a hungry smile. <i>\"Are you trying to tempt me? How could I resist such a perfect package?\"</i>";
        }if(character.nude()){
            return "Sofia runs her hands down her toned body and makes a show of spreading her pussy lips. <i>\"Do you like what you see, baby? The goal is open and waiting for your shot.\"</i>";
        }
        return "Sofia stretches her legs as you approach, drawing your attention to her muscular thighs and perfect ass. <i>\"Are you a fan of football? Time for a little one-on-one.\"</i>";
    }

    @Override
    public boolean fit() {
        return true;
    }

    @Override
    public boolean night() {
        return false;
    }

    @Override
    public void advance(int rank) {
        if(rank >= 3 && !character.has(Trait.foulqueen)){
            character.add(Trait.foulqueen);
        }
    }

    @Override
    public NPC getCharacter() {
        return character;
    }
    @Override
    public boolean checkMood(Emotion mood, int value) {
        switch(mood){
            case angry: case dominant:
                return value>=30;
            case nervous:
                return value>=80;
            default:
                return value>=50;
        }
    }
    @Override
    public float moodWeight(Emotion mood) {
        switch(mood){
            case angry: case dominant:
                return 1.2f;
            case nervous:
                return .7f;
            default:
                return 1f;
        }
    }
    @Override
    public String image() {
        return "assets/sofia_"+ character.mood.name()+".jpg";
    }

    @Override
    public void pickFeat() {
        ArrayList<Trait> available = new ArrayList<Trait>();
        for(Trait feat: Global.getFeats()){
            if(!character.has(feat)&&feat.req(character)){
                available.add(feat);
            }
        }
        if(available.isEmpty()){
            return;
        }
        character.add((Trait) available.toArray()[Global.random(available.size())]);
    }

    @Override
    public CommentGroup getComments() {
        CommentGroup comments = new CommentGroup();
        comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Come on, sexy! Pump me full of your delicious cream!\"</i>");
        comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"Ohhhh, dios mio! I'm too close!\"</i>");
        comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Oh, yes! Harder! Smack my ass with those heavy balls!\"</i>");
        comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Mmmmm, yes, fill me up!\"</i>");
        comments.put(CommentSituation.BEHIND_DOM_WIN, "<i>\"Now let's have some fun!\"</i>");
        comments.put(CommentSituation.BEHIND_DOM_LOSE, "<i>\"Quit squirming and gimme a clear shot!\"</i>");
        comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Do you want ecstasy, or agony?\"</i> Sofia purrs, pressing her knee gently, but threateningly against your jewels.");
        comments.put(CommentSituation.OTHER_CHARMED, "<i>\"Mmmmm, spread your legs, baby. Show me that big package! I promise I'll be gentle...\"</i>");
        comments.put(CommentSituation.OTHER_ENTHRALLED, "<i>\"Want a taste?\"</i> Sofia purrs in a husky whisper, a  finger softly tracing her plump lovebulge.");
        comments.put(CommentSituation.SELF_HORNY, "<i>\"You're making me so HOT!\"</i>");
        comments.put(new SkillComment(Attribute.Footballer),"<i>\"How do you like my footwork, baby?\"</i>");
        return comments;
    }

    @Override
    public CommentGroup getResponses() {
        CommentGroup comments = new CommentGroup();
        return comments;

    }

    @Override
    public int getCostumeSet() {
        return 1;
    }

    @Override
    public void declareGrudge(Character opponent, Combat c) {
        switch(Global.random(3)){
            case 0:
                character.addGrudge(opponent,Trait.sadisticmood);
                break;
            case 1:
                character.addGrudge(opponent,Trait.cheapshot);
                break;
            default:
                character.addGrudge(opponent,Trait.defensivemeasures);
                break;
        }
    }

    @Override
    public void resetOutfit() {
        character.outfit[0].clear();
        character.outfit[0].add(Clothing.bra);
        character.outfit[0].add(Clothing.tanktop);
        character.outfit[1].clear();
        character.outfit[1].add(Clothing.panties);
        character.outfit[1].add(Clothing.shorts);
    }
}
