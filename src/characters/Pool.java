package characters;

import global.Constants;

public enum Pool {
    STAMINA("Stamina", Constants.STARTINGSTAMINA),
    AROUSAL("Arousal",Constants.STARTINGAROUSAL),
    MOJO("Mojo",Constants.STARTINGMOJO),
    BATTERY("Battery",20),
    TIME("Time",12),
    ENIGMA("Enigma",7),
    FOCUS("Focus",12)
    ;
    private String label;
    private int baseCap;
    Pool(String label, int baseCap){
        this.label = label;
        this.baseCap = baseCap;
    }

    public String getLabel(){ return label; }
    public int getBaseCap(){ return baseCap; }
}
