package characters;

import Comments.CommentGroup;
import actions.Action;
import actions.Movement;
import combat.Combat;
import combat.Tag;
import daytime.Daytime;
import global.Global;
import global.Match;
import global.Modifier;
import items.Clothing;
import skills.Skill;

import java.util.HashSet;

public class Selene implements Personality{
    private NPC character;

    public Selene(){
        this.character = new NPC("Selene",ID.SELENE, 1, this);
        character.outfit[Character.OUTFITTOP].add(Clothing.lacebra);
        character.outfit[Character.OUTFITTOP].add(Clothing.ruffledress);
        character.outfit[Character.OUTFITBOTTOM].add(Clothing.lacepanties);
        character.outfit[Character.OUTFITBOTTOM].add(Clothing.skirt2);
        character.closet.add(Clothing.lacebra);
        character.closet.add(Clothing.lacepanties);
        character.closet.add(Clothing.ruffledress);
        character.closet.add(Clothing.skirt2);
        this.character.change(Modifier.normal);
        character.add(Trait.female);
        character.add(Trait.phallicappendage);
        Global.gainSkills(this.character);
        character.plan = Emotion.sneaking;
        character.mood = Emotion.confident;
        character.strategy.put(Emotion.hunting, 2);
        character.strategy.put(Emotion.sneaking, 4);

    }

    @Override
    public Skill act(HashSet<Skill> available, Combat c) {
        return null;
    }

    @Override
    public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
        return null;
    }

    @Override
    public NPC getCharacter() {
        return character;
    }

    @Override
    public void rest(int time, Daytime day) {

    }

    @Override
    public String bbLiner() {
        return null;
    }

    @Override
    public String nakedLiner() {
        return null;
    }

    @Override
    public String stunLiner() {
        return null;
    }

    @Override
    public String taunt() {
        return null;
    }

    @Override
    public void victory(Combat c, Tag flag) {
        Global.gui().displayImage("premium/Selene Tease.jpg", "Art by AimlessArt");
    }

    @Override
    public void defeat(Combat c, Tag flag) {
        Global.gui().displayImage("premium/Selene Licked.jpg", "Art by AimlessArt");
    }

    @Override
    public String victory3p(Combat c, Character target, Character assist) {
        return null;
    }

    @Override
    public String intervene3p(Combat c, Character target, Character assist) {
        return null;
    }

    @Override
    public String resist3p(Combat c, Character target, Character assist) {
        return null;
    }

    @Override
    public void watched(Combat c, Character target, Character viewer) {

    }

    @Override
    public String describe() {
        return null;
    }

    @Override
    public void draw(Combat c, Tag flag) {

    }

    @Override
    public boolean fightFlight(Character opponent) {
        return false;
    }

    @Override
    public boolean attack(Character opponent) {
        return false;
    }

    @Override
    public void ding() {

    }

    @Override
    public String startBattle(Character opponent) {
        return null;
    }

    @Override
    public boolean fit() {
        return false;
    }

    @Override
    public boolean night() {
        return false;
    }

    @Override
    public void advance(int Rank) {

    }

    @Override
    public boolean checkMood(Emotion mood, int value) {
        return false;
    }

    @Override
    public float moodWeight(Emotion mood) {
        return 0;
    }

    @Override
    public String image() {
        return null;
    }

    @Override
    public void pickFeat() {

    }

    @Override
    public CommentGroup getComments() {
        return null;
    }

    @Override
    public CommentGroup getResponses() {
        return null;
    }

    @Override
    public int getCostumeSet() {
        return 0;
    }

    @Override
    public void declareGrudge(Character opponent, Combat c) {

    }

    @Override
    public void resetOutfit() {

    }
}
