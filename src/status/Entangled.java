package status;

import characters.Attribute;
import characters.Character;
import combat.Combat;

public class Entangled extends Status {
    private Character owner;

    public Entangled(Character affected, Character owner, int magnitude){
        super("Entangled",affected);
        flag(Stsflag.entangled);
        this.owner = owner;
        this.magnitude = magnitude;
        tooltip = magnitude+"% Entangled. Negative effects will apply as this increases.";
    }
    @Override
    public String describe() {
        if(affected.human()){
            return "You are partially entangled in "+owner.name()+"'s tentacles.";
        }
        else{
            return affected.name()+" is partially wrapped in tentacles.";
        }
    }

    public int escape(){
        return -magnitude/10;
    }

    @Override
    public Status copy(Character target) {
        return new Entangled(affected,owner,magnitude);
    }

    public void turn(Combat c){
        if(c.stance.inContact()){
            magnitude += 5;
        }else{
            magnitude -= 30;
        }
        if(magnitude >= 80 && owner.getPure(Attribute.Eldritch)>=9){
            affected.add(new Bound(affected,owner.get(Attribute.Eldritch)/3,"tentacles"));
        }
        if(magnitude >= 60 && owner.getPure(Attribute.Eldritch)>=21){
            affected.add(new Hypersensitive(affected),c);
        }
        if(magnitude >= 40 && owner.getPure(Attribute.Eldritch)>=18){
            affected.add(new Horny(affected,3,2),c);
        }
        if(magnitude >= 20 && owner.getPure(Attribute.Eldritch)>=12){
            affected.add(new Oiled(affected),c);
        }
        if(magnitude<=0){
            affected.removeStatus(this);
        }
    }
}
