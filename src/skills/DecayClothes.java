package skills;

import characters.Attribute;
import characters.Character;
import characters.ID;
import characters.Pool;
import combat.Combat;
import combat.Result;

public class DecayClothes extends Skill{
    public DecayClothes(Character self) {
        super("Decay Clothes", self);
        addTag(Attribute.Unknowable);
        addTag(SkillTag.STRIPPING);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Unknowable)>=1;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canActNormally(c) && !target.nude() && self.canSpend(Pool.ENIGMA,2);
    }

    @Override
    public String describe() {
        return "Accelerates time to decay opponents' clothes off their body: 2 Enigma";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spend(Pool.ENIGMA,2);
        if(target.get(Attribute.Temporal)>=2){
            if(self.human()){
                c.write(self,deal(0, Result.miss,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.miss,target));
            }
        }else if(c.isWatching(ID.MARA)){
            if(self.human()){
                c.write(self,deal(0, Result.defended,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.defended,target));
            }
        }else{
            target.nudify();
            if(self.human()){
                c.write(self,deal(0, Result.normal,target));
            }
            else if(target.human()){
                c.write(self,receive(0, Result.normal,target));
            }
        }
    }

    @Override
    public Skill copy(Character user) {
        return new DecayClothes(user);
    }

    @Override
    public Tactics type() {
        return Tactics.stripping;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            return "";
        }else if(modifier == Result.defended){
            return "";
        }else{
            return "";
        }

    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if(modifier == Result.miss){
            return "The world seems to distort around you, and your clothes seem to suddenly be much older and brittler. " +
                    "You don't know how "+self.name()+" is manipulating time around you, but some quick adjustments to your " +
                    "Procrastinator undo the whole process.";
        }else if(modifier == Result.defended){
            return "[Placeholder: Mara assists]";
        }else{
            return "The world seems to distort around you, and your clothes seem to suddenly be much older and brittler. " +
                    "Within moments, they seem to literally crumble into dust, leaving you completely naked.";
        }
    }
}
