package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import status.Stsflag;
import status.Tranquil;

public class Tranquility extends Skill{

    public Tranquility(Character self){
        super("Tranquility",self);
        addTag(Attribute.Spirituality);
        addTag(SkillTag.DEFENSIVE);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Spirituality)>=1;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canActNormally(c) && !self.is(Stsflag.horny) && !self.is(Stsflag.mindaffecting) && !self.is(Stsflag.tranquil)
                && self.canSpend(Pool.FOCUS,3);
    }

    @Override
    public String describe() {
        return "Resist pleasure with spiritual tranquility: 3 Focus";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spend(Pool.FOCUS,3);
        if(self.human()){
            c.write(self,deal(0, Result.normal,target));
        }
        else if(target.human()){
            c.write(self,receive(0, Result.normal,target));
        }
        self.add(new Tranquil(self),c);

    }

    @Override
    public Skill copy(Character user) {
        return new Tranquility(user);
    }

    @Override
    public Tactics type() {
        return Tactics.calming;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "You let a wave of tranquility wash over you, dulling your senses.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return self.name()+" takes a deep breath. You can practically see an oasis of tranquility around "+self.pronounTarget(false)+".";
    }
}
