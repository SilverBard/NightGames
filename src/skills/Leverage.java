package skills;

import characters.Character;
import combat.Combat;
import combat.Result;

public class Leverage extends Skill {
    public Leverage(Character self) {
        super("Leverage", self);
    }

    @Override
    public boolean requirements(Character user) {
        return false;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return false;
    }

    @Override
    public String describe() {
        return null;
    }

    @Override
    public void resolve(Combat c, Character target) {

    }

    @Override
    public Skill copy(Character user) {
        return null;
    }

    @Override
    public Tactics type() {
        return null;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return null;
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return null;
    }
}
