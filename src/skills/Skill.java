package skills;
import java.util.HashSet;

import combat.Combat;
import combat.Result;
import combat.Tag;

import characters.Character;

// change start -GD
// new
public abstract class Skill implements Comparable<Skill>{
// old
//public abstract class Skill{
	/**
	 * 
	 */
	// new
	public String name;
	// old
	//protected String name;
	
// change end -GD
	protected Character self;
	protected String image;
	protected String artist;
	protected Integer sortOrder = null;
	protected HashSet<Tag> tags;
	protected int accuracy;
	protected int speed;
	public Skill(String name, Character self){
		this.name=name;
		this.self=self;
		this.image=null;
		this.artist=null;
		this.speed = 5;
		this.accuracy = 5;
		tags = new HashSet<Tag>();
	}
	public abstract boolean requirements(Character user);
	public abstract boolean usable(Combat c, Character target);
	public abstract String describe();
	public abstract void resolve(Combat c, Character target);
	public abstract Skill copy(Character user);
	public abstract Tactics type();
	public abstract String deal(int damage, Result modifier, Character target);
	public abstract String receive(int damage, Result modifier, Character target);
	public void setAccuracy(int accuracy){this.accuracy=accuracy;}
	public int accuracy(){
		return accuracy;
	}
	public void setSpeed(int speed){this.speed=speed;}
	public int speed(){
		return speed;
	}
	public String toString(){
		return name;
	}
	public Character user(){
		return self;
	}
	public void setSelf(Character self){
		this.self=self;
	}
	public void setName(String name){
		this.name=name;
	}
	public void addTag(Tag tag){tags.add(tag);}
	public boolean hasTag(Tag tag){return tags.contains(tag);}
	// change add -GD
	// new
	public Integer getSortOrder(){
		if (sortOrder != null)
			return sortOrder;
		return type().ordinal();
	}
	// change end -GD
	@Override
	public boolean equals(Object other){
		return this.getClass()==other.getClass();
	}
	// change add -GD
	// new
	@Override
	public int compareTo(Skill other){
		int ret = getSortOrder() - other.getSortOrder();
		if (ret != 0)
			return ret;
		
		return name.compareTo(other.name);
	}
	// change end -GD
}
