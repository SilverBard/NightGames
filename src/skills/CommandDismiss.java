package skills;

import characters.Character;
import combat.Combat;
import combat.Result;

public class CommandDismiss extends PlayerCommand {

	public CommandDismiss(Character self) {
		super("Force Dismiss", self);
		addTag(SkillTag.COMMAND);
	}

	public boolean usable(Combat c, Character target) {
		return super.usable(c, target) && target.pet != null;
	}

	@Override
	public String describe() {
		return "Have your thrall dismiss their pet.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		c.write(self,deal(0, Result.normal, target));
		target.pet.remove();
	}

	@Override
	public Skill copy(Character user) {
		return new CommandDismiss(user);
	}

	

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		return "You think you briefly see a pang of regret in " + target.name()
				+ "'s eyes, but she quickly dismisses her "
				+ target.pet.toString().toLowerCase() + ".";
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The"
				+ " Silver Bard: CommandDismiss-receive>>";
	}

}
