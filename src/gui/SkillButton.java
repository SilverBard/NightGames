package gui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import skills.Skill;
import skills.Tactics;

import combat.Combat;

import java.awt.Color;
import java.awt.Font;


public class SkillButton extends KeyableButton{
	protected Skill action;
	protected Combat combat;
	public SkillButton(Skill action, Combat combat){
		super(action.toString());
		setFont(new Font("Georgia", Font.PLAIN, 18));
		this.action=action;
		setToolTipText(action.describe());
		if(action.type()==Tactics.damage){
			setBackground(new Color(150,0,0));
			setForeground(Color.WHITE);
		}
		else if(action.type()==Tactics.pleasure||action.type()==Tactics.fucking){
			setBackground(Color.PINK);
		}
		else if(action.type()==Tactics.positioning){
			setBackground(new Color(0,100,0));
			setForeground(Color.WHITE);
		}
		else if(action.type()==Tactics.stripping){
			setBackground(new Color(0,100,0));
			setForeground(Color.WHITE);
		}
		else if(action.type()==Tactics.status){
			setBackground(Color.CYAN);
		}
		else if(action.type()==Tactics.recovery||action.type()==Tactics.calming){
			setBackground(Color.WHITE);
		}
		else if(action.type()==Tactics.summoning){
			setBackground(Color.YELLOW);
		}
		else{
			setBackground(new Color(200,200,200));
		}
		this.combat=combat;
		addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SkillButton.this.combat.act(SkillButton.this.action.user(), SkillButton.this.action);
			}			
		});
	}
}
