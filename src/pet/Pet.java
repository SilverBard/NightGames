package pet;

import combat.Combat;
import characters.Character;
import characters.Trait;

public abstract class Pet {
	private String name;
	private Character owner;
	private Ptype type;
	protected int power;
	protected int ac;
	
	public Pet(String name,Character owner,Ptype type, int power, int ac){
		this.owner=owner;
		this.name=name;
		this.type=type;
		this.power=power;
		this.ac=ac;
	}
	public String toString(){
		return name;
	}
	public Character owner(){
		return owner;
	}
	public String own(){
		if(owner.human()){
			return "Your ";
		}
		else{
			return owner.name()+"'s ";
		}
	}
	public abstract String describe();
	public abstract void act(Combat c, Character target);
	public abstract void vanquish(Combat c,Pet opponent);
	public abstract void caught(Combat c,Character captor);
	public abstract Trait gender();
	public void remove(){
		owner.pet=null;
	}
	public Ptype type(){
		return type;
	}
	public int power(){
		return power;
	}
	public int ac(){
		return ac;
	}
}
